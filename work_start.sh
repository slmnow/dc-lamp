secret='slmnow_0174_6676'

if [ "$2" != "" ]
then
 #Use IP entered in command line
 secret="$2"
fi

directory=www/html/$1

echo "shutting down site: $1 in directory: $directory."


/usr/bin/php $directory/artisan down --secret=$secret

sed -i 's/APP_ENV=production/APP_ENV=local/g' $directory/.env
sed -i 's/APP_DEBUG=false/APP_DEBUG=true/g' $directory/.env
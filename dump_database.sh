#!/bin/sh

echo "started"

HOST="cuescribe.com"
USER="root"
PWD='#Main$tream10174#'
#OUTFILE="backup_cuescribeDB_`date +"%m%d%y"`.sql.gz"
OUTFILE='dumped_cuescribeDB.sql.gz'

if [ "$1" != "" ]
then
   HOST="$1"
   PWD="$3"
   USER="$2"
   #OUTFILE="$4"
fi

echo "--host $HOST, --user $USER, --password $PWD"

mysqldump --column-statistics=0 --host=$HOST --port=3306 --user=$USER --password=$PWD --add-drop-database --routines --databases slmnow_scratch_6676 | \
 sed -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' | sed -e 's/DEFINER[ ]*=[ ]*[^*]*PROCEDURE/PROCEDURE/' | sed -e 's/DEFINER[ ]*=[ ]*[^*]*FUNCTION/FUNCTION/' \
 | gzip > $OUTFILE

echo "done"

#ADD this after apdating mysql
#--column-statistics=0
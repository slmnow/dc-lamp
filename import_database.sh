#!/bin/sh

echo "started"

USER='root'
PWD='tiger'
ZIPFILE="$1"
HOST=127.0.0.1
DATABASE="slmnow_scratch_6676"

if [ "$2" != "" ]
then

   PWD="$4"
   USER="$3"
   HOST=$2
fi

echo "example command: ./import_database.sh backup_cuescribeDB_060921.sql.gz 127.0.1.1 root tiger"

gunzip -c "$ZIPFILE" "${ZIPFILE%.*}" | mysql --host=$HOST --port=3306 --user=$USER --password=$PWD $DATABASE

echo "done"

#How can I remove the extension of a filename in a shell script?
#https://stackoverflow.com/questions/12152626/how-can-i-remove-the-extension-of-a-filename-in-a-shell-script/12152997

##"(Get-Content $directory\backup_cuescribeDB_routines.sql $encoding) $removeDefiner | mysql --host=127.0.0.1 --port=3306 --user=root --password=root $databaseName"
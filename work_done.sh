directory=./www/html/$1

echo "command: ./work_done.sh [existing directory in www]"
echo "searching for .env in: $directory"

sed -i 's/APP_ENV=local/APP_ENV=production/g' $directory/.env
sed -i 's/APP_DEBUG=true/APP_DEBUG=false/g' $directory/.env

/usr/bin/php $directory/artisan up
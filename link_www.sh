#!/bin/sh

#parent directory

ROOT_DIR=/work
DC_WWW_LOC=$ROOT_DIR/dc-www
DC_COMPOSE_LOC=$ROOT_DIR/dc-lamp
DC_WWW_LINK=$DC_COMPOSE_LOC/www
CERTS=$DC_COMPOSE_LOC/certs
WK_DIR_NAME=.well-known
SITE=cuescribe
SRC_WK_LOC=$DC_COMPOSE_LOC/$WK_DIR_NAME
SRC_PUBLIC_WWW=$DC_WWW_LOC/$SITE/public
WK_LINK=$SRC_PUBLIC_WWW/$WK_DIR_NAME
GETSSL=$DC_COMPOSE_LOC/.getssl

#Where soure code is placed; Same will be linked to Doc Root
mkdir $DC_WWW_LOC
echo "mkdir $DC_WWW_LOC"
 
#This is the only way i can get the link creation to work 
#This allows us to save source code outside of docker-compose

#Delete if already exist. 
rm -f $DC_WWW_LINK
ln -sf $DC_WWW_LOC $DC_WWW_LINK
echo "ln -sf $DC_WWW_LOC $DC_WWW_LINK"


#Copy certs to certs directory
cp -f $GETSSL/cuescribe.com/$SITE.com.crt $CERTS  
cp -f $GETSSL/cuescribe.com/$SITE.com.key $CERTS  
cp -f $GETSSL/cuescribe.com/chain.crt $CERTS 

#restart server
docker restart lamp-php8

#Runs from /dc-lamp
#WK = well known
#DC = docker
#SRC = source
